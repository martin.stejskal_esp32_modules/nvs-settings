idf_component_register(SRCS "nvs_settings.c"
  INCLUDE_DIRS "."
  REQUIRES nvs_flash config)
# Note: the "config" component shall include "cfg.h" (but it does not have to).
# In that file can be defined constants which can override settings in
# nvs_settings_config.h