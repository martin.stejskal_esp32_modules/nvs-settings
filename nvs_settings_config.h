/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for module that handle non-volatile-storage
 */
#ifndef __NVS_SETTINGS_CONFIG_H__
#define __NVS_SETTINGS_CONFIG_H__
// ===============================| Includes |================================
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("cfg.h")
#include "cfg.h"
#endif
#endif
// ================================| Defines |================================
// ============================| Default values |=============================
#ifndef NVS_STTNGS_VAL_NOT_EXIST_IS_OK
/**
 * @brief Define behavior when value in NVS does not exist
 *
 * If set to non-zero and value is not set yet in NVS, default value will be
 * used but returned error code from "get()" will be 0 (no error). If enabled
 * it simply suppress error when value does not exist yet.
 */
#define NVS_STTNGS_VAL_NOT_EXIST_IS_OK (1)
#endif  // NVS_STTNGS_VAL_NOT_EXIST_IS_OK
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
#endif  // __NVS_SETTINGS_CONFIG_H__
