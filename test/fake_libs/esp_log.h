#ifndef __ESP_LOG_H__
#define __ESP_LOG_H__

#define ESP_LOGW(tag, ...)
#define ESP_LOGE(tag, ...)

#endif  // __ESP_LOG_H__
