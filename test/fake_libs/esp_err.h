#ifndef __ESP_ERR_H__
#define __ESP_ERR_H__

typedef int esp_err_t;

/* Definitions for error constants. */
#define ESP_OK 0    /*!< esp_err_t value indicating success (no error) */
#define ESP_FAIL -1 /*!< Generic esp_err_t code indicating failure */

#define ESP_ERROR_CHECK(error_code)

#endif  // __ESP_ERR_H__
