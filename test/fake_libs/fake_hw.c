/**
 * @file
 * @author Martin Stejskal
 * @brief Faked functions related to HW
 */
// ===============================| Includes |================================
#include "nvs_flash.h"
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================
// Just dummy functions. Actually does not do anything here

esp_err_t nvs_flash_init(void) { return ESP_OK; }

esp_err_t nvs_flash_erase(void) { return ESP_OK; }

esp_err_t nvs_open(const char* name, nvs_open_mode_t open_mode,
                   nvs_handle_t* out_handle) {
  return ESP_OK;
}

void nvs_close(nvs_handle_t handle) {}

esp_err_t nvs_set_blob(nvs_handle_t handle, const char* key, const void* value,
                       size_t length) {
  return ESP_OK;
}

esp_err_t nvs_commit(nvs_handle_t handle) { return ESP_OK; }

esp_err_t nvs_erase_key(nvs_handle_t handle, const char* key) { return ESP_OK; }

esp_err_t nvs_get_blob(nvs_handle_t handle, const char* key, void* out_value,
                       size_t* length) {
  return ESP_OK;
}

esp_err_t nvs_erase_all(nvs_handle_t handle) { return ESP_OK; }
