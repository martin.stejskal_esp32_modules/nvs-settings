#ifndef __NVS_FLASH_H__
#define __NVS_FLASH_H__

#include <esp_err.h>
#include <stddef.h>
#include <stdint.h>

#define ESP_ERR_NVS_BASE 0x1100

#define ESP_ERR_NVS_NOT_FOUND (ESP_ERR_NVS_BASE + 0x02)
#define ESP_ERR_NVS_NO_FREE_PAGES (ESP_ERR_NVS_BASE + 0x0d)
#define ESP_ERR_NVS_NEW_VERSION_FOUND (ESP_ERR_NVS_BASE + 0x10)

#define NVS_KEY_NAME_MAX_SIZE (16)

typedef uint32_t nvs_handle_t;

typedef enum {
  NVS_READONLY, /*!< Read only */
  NVS_READWRITE /*!< Read and write */
} nvs_open_mode_t;

esp_err_t nvs_flash_init(void);
esp_err_t nvs_flash_erase(void);
esp_err_t nvs_open(const char* name, nvs_open_mode_t open_mode,
                   nvs_handle_t* out_handle);
void nvs_close(nvs_handle_t handle);

esp_err_t nvs_set_blob(nvs_handle_t handle, const char* key, const void* value,
                       size_t length);
esp_err_t nvs_commit(nvs_handle_t handle);
esp_err_t nvs_erase_key(nvs_handle_t handle, const char* key);
esp_err_t nvs_get_blob(nvs_handle_t handle, const char* key, void* out_value,
                       size_t* length);
esp_err_t nvs_erase_all(nvs_handle_t handle);

#endif  // __NVS_FLASH_H__
