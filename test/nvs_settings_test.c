/**
 * @file
 * @author Martin Stejskal
 * @brief Test cases for NVS settings module
 */
// ===============================| Includes |================================
#include "nvs_settings.h"

#include <stdint.h>
#include <stdio.h>
// ================================| Defines |================================
#define PRINT_ERR(expected_value, current_value)                         \
  printf("%s : %d : Expected value: %d | current value: %d\n", __func__, \
         __LINE__, (int)expected_value, (int)current_value)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static int32_t read_write_delete_test(void);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
int main(void) {
  int32_t i32_num_of_errors = 0;

  i32_num_of_errors += read_write_delete_test();

  if (i32_num_of_errors) {
    printf("Something went wrong. Total number of errors: %d\n",
           i32_num_of_errors);
  } else {
    printf("No error reported (^_^)\n");
  }

  return i32_num_of_errors;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static int32_t read_write_delete_test(void) {
  int32_t i32_num_of_errors = 0;

  uint8_t u8_default = 0x33;
  uint8_t u8_loaded;
  uint8_t u8_new_data = 0x55;
  char *pac_key = "Test 1";

  if (nvs_settings_get(pac_key, &u8_loaded, &u8_default, sizeof(u8_loaded))) {
    i32_num_of_errors++;
  }

  if (nvs_settings_set(pac_key, &u8_new_data, sizeof(u8_new_data))) {
    i32_num_of_errors++;
  }

  if (nvs_settings_del(pac_key)) {
    i32_num_of_errors++;
  }

  return i32_num_of_errors;
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
