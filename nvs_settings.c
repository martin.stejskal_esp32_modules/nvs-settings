/**
 * @file
 * @author Martin Stejskal
 * @brief Keep settings in non-volatile-storage memory
 */
//================================| Includes |================================
#include "nvs_settings.h"

#include <assert.h>
#include <string.h>

#include "nvs_settings_config.h"

// Standard ESP libraries
#include <esp_err.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
// ================================| Defines |================================
#define STORAGE_NAMESPACE "storage"
#define NVS_STTNGS_GET "NVS sttngs get"
#define NVS_STTNGS_SET "NVS sttngs set"
// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
//============================| Global variables |============================
//================================| Functions |===============================

// =====================| Internal function prototypes |======================
/**
 * @brief Get value from NVM
 * @param pac_key Key identificator
 * @param p_out_value Result will be written there
 * @param p_default_value Default value for case that value was not written to
 *                      EEPROM yet
 * @param pi_size Size of value in Bytes
 * @return Zero if there is no problem
 */
static te_nvs_sttngs_err _nvs_get(const char *pac_key, void *p_out_value,
                                  const void *p_default_value, size_t *pi_size);

/**
 * @brief Write key and value to NVS
 * @param pac_key Key identificator
 * @param p_in_value Key value
 * @param i_size Value size in Bytes
 * @return Zero if there is no problem
 */
static te_nvs_sttngs_err _nvs_set(const char *pac_key, const void *p_in_value,
                                  size_t i_size);

/**
 * @brief Deleted key from NVS (content will be lost)
 * @param pac_key Key identificator
 * @return Zero if there is no problem
 */
static te_nvs_sttngs_err _nvs_del(const char *pac_key);
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
te_nvs_sttngs_err nvs_settings_get(const char *pac_key, void *p_out_value,
                                   const void *p_default_value, size_t i_size) {
  size_t i_actual_size = i_size;

  te_nvs_sttngs_err e_err_code =
      _nvs_get(pac_key, p_out_value, p_default_value, &i_actual_size);

  // In case that was loaded more or less Bytes than expected, upper layer shall
  // be noticed, since it could cause unexpected undefined values in output
  // structure or it could overwrite some other variable
  if (i_actual_size != i_size) {
    e_err_code = NVS_STTNGS_SIZE_DOES_NOT_MATCH;
  }

  return e_err_code;
}

te_nvs_sttngs_err nvs_settings_set(const char *pac_key, const void *p_in_value,
                                   size_t i_size) {
  return _nvs_set(pac_key, p_in_value, i_size);
}

te_nvs_sttngs_err nvs_settings_del(const char *pac_key) {
  return _nvs_del(pac_key);
}

const char *nvs_settings_get_err_str(const te_nvs_sttngs_err e_err_code) {
  switch (e_err_code) {
    case NVS_STTNGS_OK:
      return "OK";
    case NVS_STTNGS_OPEN_ERR:
      return "open error";
    case NVS_STTNGS_READ_ERR:
      return "read error";
    case NVS_STTNGS_WRITE_ERR:
      return "write error";
    case NVS_STTNGS_VARIABLE_NOT_EXIST_YET:
      return "data not set yet";
    case NVS_STTNGS_CAN_NOT_ERASE_KEY:
      return "can not erase key";
    case NVS_STTNGS_KEY_TOO_LONG:
      return "key too long";
    case NVS_STTNGS_SIZE_DOES_NOT_MATCH:
      return "size does not match";
  }

  // This should not happen, since all cases should be covered above
  assert(0);

  // At least return empty pointer to indicate invalid data
  return NULL;
}

te_nvs_sttngs_err nvs_settings_del_all(void) {
  nvs_handle_t h_nvs;
  te_nvs_sttngs_err e_ret_code = NVS_STTNGS_OK;

  esp_err_t i_err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &h_nvs);
  if (i_err) {
    // Use default & exit function - can not read anyway
    ESP_LOGE(NVS_STTNGS_GET, "Opening NVS failed: %d", i_err);
    return NVS_STTNGS_OPEN_ERR;
  }

  i_err = nvs_erase_all(h_nvs);
  if (i_err) {
    ESP_LOGE(NVS_STTNGS_SET, "Writing to NVS failed: %d", i_err);
    e_ret_code = NVS_STTNGS_WRITE_ERR;
  } else {
    // Erase command successful. Commit changes
    i_err = nvs_commit(h_nvs);

    if (i_err) {
      e_ret_code = NVS_STTNGS_WRITE_ERR;
    }
  }
  // Close anyway
  nvs_close(h_nvs);

  return e_ret_code;
}
// ==========================| Internal functions |===========================

inline static te_nvs_sttngs_err _nvs_get(const char *pac_key, void *p_out_value,
                                         const void *p_default_value,
                                         size_t *pi_size) {
  // Pointers should not be empty
  assert(pac_key);
  assert(p_out_value);
  assert(p_default_value);
  assert(pi_size);

  // Check string length. Maximum include also terminator character -> -1
  if (strlen(pac_key) >= (NVS_KEY_NAME_MAX_SIZE - 1)) {
    return NVS_STTNGS_KEY_TOO_LONG;
  }

  // Initialize NVS
  esp_err_t i_err = nvs_flash_init();
  if (i_err == ESP_ERR_NVS_NO_FREE_PAGES ||
      i_err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    // NVS partition was truncated and needs to be erased
    // Retry nvs_flash_init
    ESP_ERROR_CHECK(nvs_flash_erase());
    i_err = nvs_flash_init();
  }
  ESP_ERROR_CHECK(i_err);

  nvs_handle_t h_nvs;
  te_nvs_sttngs_err e_ret_code = NVS_STTNGS_OK;

  i_err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &h_nvs);
  if (i_err) {
    // Use default & exit function - can not read anyway
    ESP_LOGE(NVS_STTNGS_GET, "Opening NVS failed: %d", i_err);
    return NVS_STTNGS_OPEN_ERR;
  }

  i_err = nvs_get_blob(h_nvs, pac_key, p_out_value, pi_size);
  if (i_err) {
    // Use default
    memcpy(p_out_value, p_default_value, *pi_size);

    if (i_err == ESP_ERR_NVS_NOT_FOUND) {
      ESP_LOGW(NVS_STTNGS_GET, "Reading failed - variable not set yet");
      e_ret_code = NVS_STTNGS_VARIABLE_NOT_EXIST_YET;
    } else {
      ESP_LOGE(NVS_STTNGS_GET, "Reading from NVS failed: %d", i_err);
      e_ret_code = NVS_STTNGS_READ_ERR;
    }
  }
  // Close in any case
  nvs_close(h_nvs);

#if NVS_STTNGS_VAL_NOT_EXIST_IS_OK
  if (e_ret_code == NVS_STTNGS_VARIABLE_NOT_EXIST_YET) {
    return NVS_STTNGS_OK;
  }
#endif

  return e_ret_code;
}

inline static te_nvs_sttngs_err _nvs_set(const char *pac_key,
                                         const void *p_in_value,
                                         size_t i_size) {
  // Pointers should not be empty
  assert(pac_key);
  assert(p_in_value);

  // Size should not non-zero value
  assert(i_size);

  // Check string length. Maximum include also terminator character -> -1
  if (strlen(pac_key) >= (NVS_KEY_NAME_MAX_SIZE - 1)) {
    return NVS_STTNGS_KEY_TOO_LONG;
  }

  nvs_handle_t h_nvs;
  te_nvs_sttngs_err eRetCode = NVS_STTNGS_OK;

  esp_err_t i_err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &h_nvs);
  if (i_err) {
    ESP_LOGE(NVS_STTNGS_SET, "Opening NVS failed: %d", i_err);
    return NVS_STTNGS_OPEN_ERR;
  }

  i_err = nvs_set_blob(h_nvs, pac_key, p_in_value, i_size);
  if (i_err) {
    ESP_LOGE(NVS_STTNGS_SET, "Writing to NVS failed: %d", i_err);
    eRetCode = NVS_STTNGS_WRITE_ERR;
  } else {
    // Commit write
    i_err = nvs_commit(h_nvs);
    if (i_err) {
      eRetCode = NVS_STTNGS_WRITE_ERR;
    }
  }
  // Close anyway
  nvs_close(h_nvs);

  return eRetCode;
}

inline static te_nvs_sttngs_err _nvs_del(const char *pac_key) {
  assert(pac_key);

  // Check string length. Maximum include also terminator character -> -1
  if (strlen(pac_key) >= (NVS_KEY_NAME_MAX_SIZE - 1)) {
    return NVS_STTNGS_KEY_TOO_LONG;
  }

  nvs_handle_t h_nvs;
  te_nvs_sttngs_err e_ret_code = NVS_STTNGS_OK;

  esp_err_t i_err = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &h_nvs);
  if (i_err) {
    ESP_LOGE(NVS_STTNGS_SET, "Opening NVS failed: %d", i_err);
    return NVS_STTNGS_OPEN_ERR;
  }

  i_err = nvs_erase_key(h_nvs, pac_key);

  if (i_err == ESP_ERR_NVS_NOT_FOUND) {
    // Key does not exists -> kind of OK
    e_ret_code = NVS_STTNGS_OK;
  } else if (i_err == ESP_OK) {
    // Delete successful. Commit changes
    i_err = nvs_commit(h_nvs);
    if (i_err) {
      e_ret_code = NVS_STTNGS_WRITE_ERR;
    }
  } else if (i_err != ESP_OK) {
    // Some another error
    e_ret_code = NVS_STTNGS_CAN_NOT_ERASE_KEY;
  }

  // Close anyway
  nvs_close(h_nvs);

  return e_ret_code;
}
