/**
 * @file
 * @author Martin Stejskal
 * @brief Keep settings in non-volatile-storage memory
 */
#ifndef __NVS_SETTINGS_H__
#define __NVS_SETTINGS_H__
//================================| Includes |================================
#include <stddef.h>

// Modules from which structures are taken
// ================================| Defines |================================

// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
typedef enum {
  NVS_STTNGS_OK = 0,     //!< OK
  NVS_STTNGS_OPEN_ERR,   //!< Problem with opening NVS
  NVS_STTNGS_READ_ERR,   //!< Read error
  NVS_STTNGS_WRITE_ERR,  //!< Write error

  /// Variable not set yet. Use save function to write into memory
  NVS_STTNGS_VARIABLE_NOT_EXIST_YET,

  NVS_STTNGS_CAN_NOT_ERASE_KEY,  //!< Can not deleted record for given key
  NVS_STTNGS_KEY_TOO_LONG,       //!< Key string is too long

  /// Requested size does not match with what was loaded or saved
  NVS_STTNGS_SIZE_DOES_NOT_MATCH,
} te_nvs_sttngs_err;

//============================| Global variables |============================
//================================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Load setting from NVS
 * @param pac_key Unique key identificator as string
 * @param p_out_value Pointer to memory where loaded data will be written
 * @param p_default_value Pointer to default data. Default data will be used
 *                        in case that key is not registered yet, so at least
 *                        expected data will be populated to output variable.
 * @param i_size Size of structure/array which handle data. Size is in Bytes
 * @return NVS_STTNGS_OK if no error
 */
te_nvs_sttngs_err nvs_settings_get(const char *pac_key, void *p_out_value,
                                   const void *p_default_value, size_t i_size);

/**
 * @brief Set settings to NVS
 * @param pac_key Unique key identificator as string
 * @param p_in_value Pointer to memory from where data will be loaded
 * @param i_size Size of data to be stored
 * @return NVS_STTNGS_OK if no error
 */
te_nvs_sttngs_err nvs_settings_set(const char *pac_key, const void *p_in_value,
                                   size_t i_size);

/**
 * @brief Delete settings from NVS
 * @param pac_key Unique key identificator as string
 * @return NVS_STTNGS_OK if no error
 */
te_nvs_sttngs_err nvs_settings_del(const char *pac_key);

/**
 * @brief Delete all settings from NVS
 *
 * @warning This might remove all data from NVS! Make sure that there is no
 *          other module that is using NVS as well!
 *
 * @return NVS_STTNGS_OK if no error
 */
te_nvs_sttngs_err nvs_settings_del_all(void);

/**
 * @brief Convert error code number into string
 *
 * @param e_err_code Error code as string
 * @return Pointer to the string which interprets input error code
 */
const char *nvs_settings_get_err_str(const te_nvs_sttngs_err e_err_code);
#endif  // __NVS_SETTINGS_H__
